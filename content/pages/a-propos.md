Title: À propos
Date: 2020-04-05 10:20
Modified: 2020-03-30 10:20
Category: misc
Slug: a-propos
Summary: About this website

> "It is always the Year One here. Only the dating of every past and future year changes each New Year’s Day, as one counts backwards or forwards from the unitary Now."

>Ursula K. Le Guin, *The Left Hand of Darkness*, 1969--




Les chroniques des jours meilleurs est un recueil d'images, de sons et de textes sur l'urgence. Il a pris forme pendant le confinement lié à la pandémie de Covid-19 en 2020. Voilà les pensées qui l’accompagnent. Elles ne sont ni nécessaires à sa compréhension, ni réductrices de son sens.


Prévoir, c’est attendre. C’est aussi, de fait, faire une division entre maintenant et plus tard, et par la même occasion, échapper à l’ininterruption des jours. Je passe une grande partie de mon présent à attendre.
Ces derniers temps m’ont fait ressentir une forme d’urgence. Elle compresse le futur incertain avec le désemparement du présent, elle met un coup de projecteur sur leur enchaînement. Je ne parle pas de l’urgence d’un danger imminent ou d’une pression extérieure. C’est une urgence intérieure, sécurisée, ce qui la rend aussi muette et évitable.
Le paradoxe est que cette urgence n’enjoint pas à s’activer, à faire plus de choses, plus vite. Elle rend même cette pulsion assez dérisoire. Elle n’enjoint pas plus à ne rien faire ou à faire les choses lentement. Elle apporte autant d’inquiétude que de sérénité. Il n’y a pas plus de raison d’attendre un futur incertain que des jours meilleurs. Au fond, il n’y a pas beaucoup de raisons d’attendre.


Site de flemar et theo, qui carbure à [Pelican](https://blog.getpelican.com/).

Fonts : [Solide Mirage](https://www.velvetyne.fr/fonts/solide-mirage/), [M+](http://mplus-fonts.osdn.jp/)

Hébergé par theo sur OVH.