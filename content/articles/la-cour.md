Title: La cour
Date: 2020-04-24 17:01
Modified: 2020-04-24 17:01
Category: public
Slug: la-cour
Authors: felicie
Summary: A man and a building.

<audio controls="controls" 
    src="sounds/courtyard.wav"
    autoplay loop
    class='audio'>
    Your browser does not support the HTML5 Audio element.
</audio>

Il fait moins attention à l’extériorité de ses émotions.  
L’oiseau qui s’est envolé juste à côté de lui l’a surpris, ça a secoué son corps. Ses oreilles fixaient le son obstiné d’un couteau sur une planche à découper. Mais c’est sa réaction davantage que l’oiseau qui l’a vraiment surpris. Après tout, les oiseaux s’envolent assez régulièrement.  
Il s’efforçait généralement d’être aussi réservé dans son espace privé qu’il le serait avec d’autres personnes. En inspectant attentivement cette habitude, il s’était demandé si cela aurait semblé déraisonnable à quelqu’un d’autre. Il craignait fondamentalement que le fait de devenir soudainement extraverti en étant seul sans que le reste du monde soit au courant aurait fait de lui quelqu’un de malhonnête.  
Et maintenant il commençait à réaliser, alors qu’il se faisait face à la longueur des journées, qu’il avait besoin d’une certaine forme de confrontation pour remplacer le contact social le plus basique. Il avait besoin que quelque chose ou quelqu’un aille à son encontre, même si c'était simplement pour lui faire remarquer qu’il n’allait pas si mal que ça. Une forme de conversation, qui ramènerait une dimension dialectique à la vie.

Il n’apprécie pas beaucoup les appels téléphoniques, du moins pas autant qu’il en aurait besoin. La faille géographique qu’ils créent le mettent mal à l’aise. Il cherche des manière de cacher à l’inconfort qu’il ressent en entendant quelqu’un qui, pour ce qu’il en sait, pourrait tout aussi bien être sur une autre planète. Mais ce n’est pas si simple.  
Avec un soin démesuré, il dessine des lignes parallèles. Quand les deux côtés du papier sont remplis, il choisit une autre couleur et trace de nouvelles lignes entre les premières. À ce moment-là, la conversation devient illusoire et son esprit commence à mettre au point une excuse pour raccrocher,  ainsi qu’un plan d’action pour rendre cette excuse la plus légitime possible une fois la conversation terminée, afin qu’il ne se sente pas trop malhonnête.  
Au couché du soleil, il commence à envisager avec une forme de terreur que l’angle de la lumière sur la portion de Terre qu’il habite est différent de celui sur la portion de Terre à l’autre bout du fil. Il n’apprécie vraiment pas beaucoup les appels téléphoniques.

Il rit à haute voix régulièrement et ça sonne bizarre. À la fois libéré et consciencieux. Comme si cela faisait partie d’une vraie conversation, il ressent souvent par la suite le besoin d’exprimer, à voix haute, ce qui était si drôle. La brosse à dent au bord du lavabo a l’air un peu perdue mais si cela lui permet d’être en paix avec lui-même, elle est là pour aider.  
Il commente également avec mépris ou intérêt, chaque chose qu’il entreprend. Il lui arrive même de se répondre, ce qu’il lui avait toujours apparu comme le point de non-retour. Mais avoir le dernier mot est devenu une question de principe.

Deux fois par jour, il fume une cigarette, au balcon qui donne sur la cour. C’est juste assez à l’extérieur pour qu’il ait l’impression d’aller quelque part. Le bazar et la poussière qui traînaient sur les autres balcons ont fait de l’espace pour accueillir une chaise et une personne lisant au le soleil.  
Il entend les voisins rire à voix hautes, comme ils le font toujours les après-midi de printemps. Ils ont commencé à regarder une vieille série la semaine dernière. Il connaît la fin et peut même répéter pour lui-même quelques répliques.