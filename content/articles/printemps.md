Title: Le retour du printemps
Date: 2020-04-24 17:01
Modified: 2020-04-24 17:01
Category: public
Slug: printemps
Authors: felicie
Summary: Le printemps revient.

<audio controls="controls"
    src="sounds/printemps.wav"
    autoplay loop
    class='audio'>
    Your browser does not support the HTML5 Audio element.
</audio>

Elle se dit, non mais c’est pas possible il en fait trop. On dirait que le ciel va exploser, et pourtant sa sœur penche à la fenêtre son sourire ravi, et ce rire qui n’éclate qu’au retour du printemps. Elle se dit, c’est un peu fort quand même, on dirait que ça fait des années qu’on l’attend, le mec se pointe du jour au lendemain comme une fleur.  
Elle a une horreur sainte mais silencieuse de ces moments dont l’appréciation est consensuelle. La météo ne fait jamais de vague, on nous dit que les citoyens sont épanouis sous un soleil radieux. Et les nuages et les vents ne sont que de passage. Retour à la normal. Retour du printemps.

Le lendemain il pleut sans surprise. Sans surprise sa sœur s’installe à la fenêtre encore, emballée dans un plaid comme un cadeau dont on cherche à cacher la forme. Elle essuie patiemment avec son index la buée sur un carreau, en partant du coin en haut à droite sur des bandes de cinq centimètres. Ça fait des traces sur les vitres, la régularité est jouissive.

Non mais dites-moi, c’est pas bientôt fini cette histoire. On se fait avoir à chaque fois, c’est quand même un comble. Elle tourne en rond dans la cuisine en attendant que la bouilloire ait fini son ronronnement vaporeux. Par moments, elle s’emporte un peu et se met à tourner en ovale. Elle a préparé la théière, le thé, le sucre au cas où vraiment, elle ne s’en sorte pas.

On entend les fleurs pousser dehors. Le lilas sur les plates-bandes du voisin qui se fait un devoir consciencieux de restaurer un bout de biodiversité choisie et bien rangée. L’odeur est un peu trop évidente à son goût, et par-dessus le marché, les oiseaux chantent.  
Sa sœur essaie de prendre des bonnes habitudes. C’est 21 jours, répète-t-elle depuis 4 jours, 21 jours et après biologiquement ça se fait tout seul. Biologiquement, elle manque pas d’air tiens.

Le temps que l’agacement se dissipe, elle fait une ligne avec des graviers sur le rebord de la fenêtre. Demain la pluie tombera de nouveau et les emportera en bas, dans la cour.