window.onload = function(){
    // close on esc key
    document.addEventListener('keydown', function(event){
        if(event.keyCode === 27){
            closeModal()
        }

    });
}

// Close the Modal
function closeModal() {
    document.getElementById("slide-container").style.display = "none"
    elems = document.getElementsByClassName('slide')
    for(let i=0; i<elems.length; i++){
        elems[i].style.display = "none"
    }
    document.getElementsByTagName("header")[0].style.opacity = 1
    document.getElementById("a-propos").style.opacity = 1
    document.getElementById("telecharger").style.opacity = 1
}

function showSlide(id){
    closeModal()
    isOpen = true
    document.getElementById('slide-container').style.display = "block"
    document.getElementById(id).style.display = "block"
    // change persistent elements' opacity
    document.getElementsByTagName("header")[0].style.opacity = 0.2
    document.getElementById("a-propos").style.opacity = 0.2
    document.getElementById("telecharger").style.opacity = 0.2
    goToTop()
}

// When the user clicks on an image, scroll to the top
function goToTop() {
    document.body.scrollTop = 0 //  safari
    document.documentElement.scrollTop = 0 // Chrome, Firefox, IE and Opera
} 