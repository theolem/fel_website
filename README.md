# Better days

Un site par Félicie en utilisant Pelican. 
Accessible sur [Better days](https://betterdays.theo-lem.org/index.html). 

## Règles du git
**Pas de binaires !!!** On évite donc de mettre sur le git les fichiers suivants : 
* images
* sons
* font (TODO : les retirer de l'historique du git). 

Ils doivent être téléversés sur le serveur directement. 

**Nommage des fichiers** : Pour être pris en compte dans le site final, les fichiers doivent être nommés de la manière suivante (attention à la casse) : 
* sons : nom-de-l'article-sans-majuscule->.wav|mp3 (TODO : les mettre en .mp3 plutôt qu'en .wav, et les placer dans le template plutôt que le .md)
* images : Nom de l article-x.jpg
* contenu : chaque nouvel article doit contenir les champs suivants au début : 
```
Title: 
Date: (date de publication, format : 2020-04-05 10:20)
Modified: (date de modification, idem)
Category: ("article" pour les articles avec texte, "gallery" si seulement des images)
Slug: (version sans espace ni caractère spécial du titre, par exemple "mon-titre-d'article". Sera repris dans l'URL de l'article.)
Summary: (un rapide sommaire, optionnel)
```