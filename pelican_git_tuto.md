# Faire un site avec Git et Pelican

[toc]

_Ce tuto est destiné aux débutant⋅e⋅s souhaitant réaliser un site avec Pelican et Git, deux outils très utiles. Il est écrit à l'arrache, mais il vise au maximum à être partagé et corrigé. J'ai notamment voulu utiliser l'écriture inclusive, mais les oublis sont sans doute légions, et je m'en excuse._

## Les outils 

Pour faire ce site on va utiliser deux outils : 

* **Pelican**, un générateur de site statique
* **Git**, un outil de contrôle de version de code.

L'utilité de ces deux outils est expliquée plus bas. 

On peut aussi utiliser (fortement recommandé) un éditeur de texte orienté code : VScode, Atom, Geany... ou pour les malades Vim. 

## Pelican 

### 1. Un site statique, c'est quoi ? 

Pelican est un **générateur de site statique**, et [ce n'est pas le seul](https://www.staticgen.com/). Un site statique, pour faire court, c'est un site très simple et dépouillé, qui est constitué de fichiers HTML (+CSS et Javascript, qui l'accompagnent quasiment toujours), et qui fonctionne uniquement avec un serveur HTTP (ex : Apache, NGINX), autrement dit **aucun langage de script** n'est utilisé pour la réalisation de ce site. Il n'y aura donc pas de serveur codé pour ce site, en PHP, Python, NodeJS, Go... Le site ressemblera simplement à un dossier, dans lequel on trouvera des fichiers HTML et des ressources pour aller avec (CSS, images, sons... )

Pour faire simple, on va se poser deux questions, en se mettant du point de vue du **visiteur du site** : 

* **que peut faire un site statique ? ** Il peut afficher des pages HTML avec des feuilles de styles. On aura des liens entre ces feuilles de styles, on pourra avoir des catégories de pages (par exemple, on peut avoir un blog et une gallerie d'image sur le même site), des labels sur les pages, et même des flux RSS/Atom. Le nombre de fonctionnalité dépend du générateur de site, qui peut aller du très complet au très dépouillé. 
* **qu'est-ce qu'il ne pourra pas faire ?** Il ne pourra pas gérer de comptes utilisateurs, de commentaires, une page d'administration du site... la règle est la suivante : ce qui s'affiche pour un utilisateur s'affichera de manière identique pour tous les autres. 

Autrement dit, un site statique sert surtout pour un site _vitrine_, où le contenu ne s'adapte pas à l'utilsateur. Ça peut sembler faiblard comme ça, mais malgré ses limites il y a beaucoup d'avantages : faible consommation énergétique, légèreté des pages qui fluidifie la navigation, et surtout facilité de prise en main. 

### 2. Concrètement, ça ressemble à quoi ? 

Comme dit plus haut, votre site web Pelican tient dans un dossier, avec une architecture simple. Les dossiers présents seront les suivants : 

├── content 			: le contenu du site, qu'on répartira par convention dans les dossiers appropriés
│   ├── articles
│   ├── images
│   └── pages
├── Makefile 		: un fichier qui automatise la publication du site
├── output			: ton site final, tel qu'il sera visible par le public ! wouhou ! 
├── pelicanconf.py		: 
├── publishconf.py		:
├── tasks.py				  :  trois fichiers de configuration (.py car Pelican est écrit en Python)
└── themes					: l'apparence de votre site 
    └── montheme
        ├── static
        │   └── css			: les feuilles de styles. On peut aussi trouver un fichier "font" ici, avec les fichiers de typo. 
        └── templates		: la structure de votre site. 



### 3. Comment je me le procure ? 

Pour commencer avec Pelican, il faut commencer par [l'installer](https://docs.getpelican.com/en/stable/install.html). Comme Pelican est écrit en Python, il est d'installer `python` (l'_interpréteur_ Python) et `pip` (le gestionnaire de paquets de Python, qui permet d'installer des centaines de modules différents). Voici quelques liens [pour Windows](https://matthewhorne.me/how-to-install-python-and-pip-on-windows-10/), [pour Mac](https://ahmadawais.com/install-pip-macos-os-x-python/), et pour Linux c'est trop facile, tu trouveras tout⋅e seul⋅e ^^

_Attention, Pelican nécessite à présent d'installer python3, et le gestionnaire de paquet qui va avec, pip3._

Maintenant que tu as Python et Pip, il faut installer Pelican ( [ici](https://docs.getpelican.com/en/stable/install.html) je te dis). Il est aussi nécessaire le packets Markdown pour pouvoir rédiger du contenu dans ce langage. Pour celà, il faut lancer la commande `pip3 install pelican[Markdown]`, qui ira chercher les bons paquets (une fois `pip3 `correctement installé).

Une fois que c'est fait, vérifier en ouvrant un terminal qu'on a accès aux commandes `pelican` et `pelican content` (les taper sur le terminal, faire entrer, et s'assurer qu'il se passe quelque chose qui ne ressemble pas à « command not found »).

Pour finir, on va commencer notre projet de site : se déplacer dans un nouveau dossier avec le terminal ([tuto pour mac](https://macpaw.com/fr/how-to/use-terminal-on-mac)), et exécuter la commande `pelican-quickstart`. Il y aura ensuite une série de questions à répondre, dans le doute répondre » Non ». _Note : si tu as déjà un projet Pélican sur lequel tu veux travailler, saute ce dernier paragraphe_. 

Maintenant, les dossiers décrits ci-dessus devraient apparaître (à l'exception de themes/ et des sous-dossiers de content/, qu'on créera plus tard).

### 4. Comment on rédige du contenu avec Pelican ? 

Pelican, comme beaucoup de générateurs de site statiques, permet de rédiger du texte dans un format beaucoup plus simple à manipuler que du HTML : le Markdown. Pourquoi ? Cellui qui pose cette question n'a sans doute jamais réellement manipulé une grande quantitié de HTML. Pour comprendre la complexité que ça peut représenter, on peut aller sur à peu près n'importe quelle page sur Internet, et faire un clic droit - afficher la source de la page. Joyeux bazar, non ? 

Le Mardown est un langage qui permet de facilement mettre en page du contenu. Quand on parle de mettre en page, ça peut être les actions suivantes : insérer un titre, un hyperlien, une image, mettre du texte en italique ou en gras, sauter une ligne ou tout simplement rajouter un paragraphe. Le HTML est très verbeux : par exemple quand on veut rajouter un paragraphe, on sera forcé d'ajouter une balise \<p>, pour une nouvelle ligne un \<br>, etc, ce qui va vite polluer visuellement notre fichier. Avec le markdown, rien de tout ça. 

Voici un exemple d'à quoi peut ressembler un fichier markdown. 

```markdown
# Mon premier titre 
## Première sous partie

Ceci est du contenu, que je peux mettre **en gras**, _en italique_. 
Je peux également écrire `du code préformaté`, et si j'ai besoin d'une citation : 

> je l'indique comme ça.

## Deuxième sous partie

Je peux également insérer des [hyperliens](isitmonday.today), voir des émojis :thinking:. 
```

Plutôt facile à lire non ? Plus que du HTML en tout cas[^1].

Voici donc la première facette de Pelican : le contenu. Il sera placé dans le dossier `content`, dans des fichiers `monfichier.md`. 

On insérera en haut du fichier les métadonnées suivantes sur l'article : 

```markdown
Title: mon titre d'article
Date: 2020-04-24 17:01 (date de publication)
Modified: 2020-04-24 17:01 (date de la dernière modification)
Category: public 	(ma catégorie de contenu)
Tags: article		(mes labels)
Slug: mon-titre-d-article		(un titre dépouillé, sans espace ni caractère spécial)
Authors: moi			(le nom de l'auteur)			
```

Puis on peut rédiger le contenu ! [Wouhou](https://www.youtube.com/watch?v=biJq3CSBQ4s) ! 

### 5. Les templates

Un deuxième aspect de Pelican sont les templates. Celles-ci sont placées dans un dossier de thèmes, qui vous faudra [créer dans un premier temps](https://docs.getpelican.com/en/stable/themes.html) s'il n'existe pas déjà. 

Les templates, c'est la structure du site. Elles permettent d'indiquer pour chaque page : 

* ou placer le contenu rédigé dans le markdown (un corps d'article de blog, par exemple)
* sous quel format devrait s'afficher le contenu en fonction de son type. On pourra par exemple faire s'afficher différement un article (pour lequel on veut afficher l'auteur) d'une page de contact générale du site, alors même que le contenu est écrit au format Markdown dans les deux cas. 
* quels éléments doivent s'afficher sur toutes les pages du site (par exemple, un en-tête et un pied de page, un logo, un titre d'onglet, une [favicon](https://fr.wikipedia.org/wiki/Favicon)). 

Bref, les templates, c'est le nerf de la guerre. C'est a priori <u>le seul endroit</u> où tu devras écrire du HTML dans ton site, mais leur rédaction peut être très simple pour autant.

Il faudra commencer par créer les fichiers vides suivants dans le dossier `theme/montheme/templates`: 

`archive.html  authors.html     category.html  period_archives.html
article.html  base.html        index.html     tag.html
author.html   categories.html  page.html      tags.html`

Beaucoup pourront rester vide pour un site simple, mais l'existence des fichiers est nécessaire pour que Pelican puisse générer le site. 

Le principe des templates est simple : tu vas écrire du « HTML à trou », et Pelican remplira les trous avec le contenu situé dans le dossier `content`. Tout le HTML du contenu sera généré par Pelican à partir du Markdown, donc tu n'auras pas à l'écrire, et tu l'en remercieras ! 

Une template correspondra à un type de page du site. Par exemple, la template `article.html` sera utilisée pour générer toutes les pages d'articles, et la template `index.html` pour la page d'accueil du site. 

Alors on les écrit, c'est templates ? 

Commençont par ouvrir `article.html`, et à insérer une structure HTML valide, comme suit : 

```html
<!DOCTYPE html>
<html lang="fr">
    <header>
        <meta charset="utf-8"> 
        <title>Le meilleur site d'internet</title>
    </header>
    <body>
        <!-- Le contenu de la page ira ici-->
    </body>
</html>
```



Ensuite, il faut dire à Pelican **quel contenu insérer**, et dans **quel élément HTML**. Ce n'est pas clair ? Regarde l'exemple suivant, qui est une version complêtée de `article.html` : 

```html
<!DOCTYPE html>
<html lang="fr">
    <header>
        <meta charset="utf-8"> 
        <title>Le meilleur site d'internet</title>
    </header>
    <body>
        <!-- Le contenu de l'article -->
        <h1> {{ article.title }} </h1>
        <p>
            {{ article.content }}
        </p>
    </body>
</html>
```

Décortiquons cet exemple  : 

1. la structure du HTML n'a pas bougé : il s'agit d'une page HTML classique.  
2. Puis arrive cette ligne : `<h1> {{ article.title }} </h1>`, qui ne ressemble pas à du HTML. C'est là qu'une template est du _HTML à trou_ : Pelican va repérer ces mentions entre doubles-crochets, et les remplacer par la variable à l'intérieur des crochets. En l'occurence, il s'agit du titre de l'article, puisqu'on est sur la page d'un article. Et comme ce titre est contenu dans un élément HTML de type `h1`, le titre de l'article s'affichera sur la page comme un titre de niveau 1. 
3. Idem pour le contenu de l'article : on a inséré avec `{{ article.content }}` le contenu de l'article dans un élément HTML `p`, c'est-à-dire un paragraphe. 

Pour résumer, Pelican va 1. **générer du HTML à partir du Markdown ** en lisant le contenu, 2. **remplir les trous** dans les templates, et 3. **tout copier dans `output`**, ce qu'on verra dans le point 7.  

Les variables suivantes sont accessibles pour les articles :  `.title , .content , .url , .author , .date ` , qui corrrespondent respectivement au titre, contenu, hyperlien, auteur et date de publication de l'article. 

En fonction des templates, les variables disponibles ne sont pas les même. On trouvera plus d'information sur l'écriture de templates dans [la documentation Pelican](https://docs.getpelican.com/en/stable/themes.html#templates-and-variables). 



### 6. L'apparence

L'apparence du site peut être maîtrisée avec des fiches de styles CSS, aussi dans le dossier `themes`. Pour cela, il suffit d'insérer dans les templates un lien vers la fiche de style voulue, de la manière suivante : 

```html
<head>
    <link rel="stylesheet" href="lien/vers/mon/fichier.css"/>
</head>
```

Je ne m'étendrai pas sur les possibilités du CSS ici, mais avoir des connaissances de base dans ce langage est important, et permet de rendre le site classe. 

### 7. Générer son site

Dernière étape pour obtenir un site : laisser Pelican faire sa magie. 

Pour ce faire, il suffit de se placer dans le dossier de base de son projet Pelican, et de taper la commande suivante : `pelican content`. Si on utilise un theme définit par soi-même comme ici, il sera probablement nécessaire de taper une commande plus complète en attendant configurer Pelican comme il faut : 

```bash
pelican content -t theme/montheme -s pelicanconf.py
```

Il ne s'est rien passé ? Détrompe-toi. Essayes de lister les fichiers dans `output/` : normalement, ton site devrait s'y trouver : des fichiers HTML correspondant à tes articles, à ta page d'accueil... le tout à partir bien sûr de ton contenu et de tes templates ! 

Mais comment accéder à ce site ? Plusieurs options là aussi : 

1. `pelican --listen` : la plus simple des options est d'utilisé le serveur HTTP minimaliste fourni avec Pelican. Pour ce faire, se placer dans le dossier de son projet Pelican, et taper la commande suivante : 

   ```bash
   pelican --listen
   ```

   Tu peux alors accéder à votre site en local sur ton navigateur, à l'adresse `localhost:8000`. La commande `pelican --autoreload --listen` est également utilse pour travailler : elle permet de re-régénérer le site à chaque modification : il n'y a plus qu'à rafraîchir la page sur son navigateur une fois le contenu, le template ou le CSS modifié. 

2. `plugin Live Server` : sur VSCode, le plugin [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) permet de faire sensiblement la même chose que `pelican --listen`, mais avec tout type de fichier HTML. Pour l'utilser, après l'avoir installé, il suffit de se rendre sur le fichier HTML qu'on souhaite visualiser, et de cliquer sur « Go live », en bas à droite de l'écran. L'intérêt est qu'à chaque modification de ce fichier, le navigateur se rafraîchit automatiquement. 

   _NB : si tu as bien suivi, le fichier que tu souhaites visualiser dans ce cas là se trouve forcément dans `output/`, et non pas dans `templates` : on ne veut pas visualiser le « HTML à trou », mais bien le HTML complet._

3. Pour les fous : installer un serveur HTTP en local (Apache ou NGINX) pour tester le site en conditions quasi-réelles. Pas très utile pour être honnête. 

Voilà tout ce qu'il faut pour utiliser Pelican ! 



## Git 

Git est un outil de gestion de version de code, qui mériterai largement un tuto à lui tout seul. En fait on peut passer sa vie à apprendre des nouvelles choses sur git, mais il est assez simple de commencer à l'utiliser pour des cas basiques. 

En bref, voilà ce que permet git : 

* **collaborer à plusieurs sur un base de code** : git permet de partager ses modifications de code avec d'autres collaborateurs, voir quand on commence à bien le maîtriser de gérer les conflits entre des modifications concurrentes, de faciliter la revue du code des autres, etc.
*   **gérer plusieurs versions du code** : git permet de conserver un _historique des modifications_ du code, ce qui permet notamment de retourner en arrière quand on a tout cassé, ou de repérer quand une modification précise a été insérée dans la base de code. 
* **conserver du code à plusieurs endroits, notamment dans le nuage** : l'utilisation de git s'accompagne le plus souvent de l'utilisation d'une _forge git_ comme Github ou Gitlab, qui sert de serveur central au code de tous les collaborateurs. Un peu comme un entrepôt où on placerai les outils et le fruit d'une journée de travail, à un endroit où d'autres personnes peuvent le trouver facilement. 

Les projets de code dans git sont stockés dans des **répos** (_repository_). Chaque modification est appelée **commit**. Chaque utilisateur peut **pousser** (soumettre) ou **tirer** (récupérer) ses modifications (commits donc) sur les répos en ligne. 

### 1. Installer git

Y'a plein de tuto sur internet, t'es grand⋅e ! (protip : chercher « install git » + le nom de ton système d'exploitation sur le net). 

_Note pour la suite : on travaillera principalement sur la ligne de commande par la suite, et quand on travaille sur du code il est courant de garder un terminal ouvert pour ces actions. De nombreux outils existent pour git avec une interface graphique et qui permettent de faire la même chose, c'est une affaire de préférence. VScode par exemple [intègre git par défaut](https://code.visualstudio.com/Docs/editor/versioncontrol), et permet de faire toutes les actions décrites plus bas de manière graphique._ 

### 2. Récupérer un répo git existant

Une première étape va être de récupérer le projet en ligne sur lequel on souhaite travailler. Pour cela, il faut se rendre sur la page sur répo en ligne, et chercher un lien «cloner le répo ». Le plus souvent, on aura le choix entre « cloner avec HTTPS » et « cloner avec SSH », dans quel cas on choisira le premier lien. **Copier ce lien**. 

Ensuite, il faut se déplacer dans le dossier où on souhaite placer son projet de code par le terminal, et taper la commande suivante en plaçant l'URL du git au bon endroit : 

```bash
git clone https://<lien-du-git>
```

Et... c'est tout ! Le dossier avec tout le code devrait s'être créé. Tu viens de créer ton **répo local**, et c'est doublement super parce que tu as accès non seulement à la dernière version du code, mais à tout l'historique des versions. [Plutôt cool](https://www.youtube.com/watch?v=biJq3CSBQ4s).  

### 3. Récupérer une modification 

Voici un cas de figure simple : tu travaille avec un certain nombre d'autres collaborateurices sur un projet. Comme vous êtes des gens biens, vous utilisez git, avec un répo en ligne. Tu as cloné le répo git en fin de journée, puis tu es parti⋅e faire autre chose. Le lendemain, après un réveil difficile, tu arrives le⋅a dernie⋅re à la réunion d'équipe, et tu apprends que [Jamie](https://www.youtube.com/watch?v=biJq3CSBQ4s) a soumis une modification (...commit, t'as compris) sur le répo en ligne. Comment fais-tu pour la récupérer ? 

Une commande très simple permet de faire ça. Après s'être déplacé⋅e dans le dossier du répo, tu as accès à la commande : 

```bash
git pull origin master
```

Quelques messages devraient s'apparaître... et c'est tout, la dernière version du code devrait être téléchargée. Eh oui Jamie ! 

Pour éviter les conflits entre les différentes versions du code, il est recommandé de lancer un `git pull` avant toute section de travaille, afin d'éviter d'appliquer des modifications sur une version du code qui n'est pas la dernière.



### 4. Soumettre ses modifications (version simple)

_Note : pour soumettre ses modifications, il faut être utilisateur de la forge git sur laquelle se trouve le répo, et avoir les droits nécessaires. Sinon n'importe qui pourrait modifier le code, et pas génial !_

Maintenant, tu vas impressioner Jamie avec ton propre commit. Une fois que tu as modifié les fichiers comme tu voulais et que tu les as sauvegardé, la série de commande suivante permet d'envoyer toutes tes modifications au répo git de la forge, pour que tout le monde puisse y avoir accès. 

```bash
git add .
git commit -m "Ici, je peux mettre un message qui décrit les modifications que j'ai faites."
git push origin master
```

La ligne de commande devrait te demander d'entrer ton nom d'utilisateurice et ton mot de passe sur la forge git. Une fois que c'est fait, si tout va bien tes modifications devraient être poussées, et disponible pour les autres !



### 5. Voir l'historique des modifications

Une dernière chose qu'on peut vouloir faire avec git est d'aller chercher quelles modifications ont été faites à quel moment. Pour cela, une commande permet d'aller chercher les messages de commit : 

```bash
git log --oneline
```

D'où l'intérêt d'avoir des messages de commit clairs et précis ! 



### 6. Si jamais ça foire

Quand on utilise git, un problème récurrent est le conflit de version. Cela se passe quand on travaille la dernière version du code en local ne correspond pas à celle en ligne, et qu'on effectue un `git pull`. Et là, c'est le drame. **Et ça t'arriveras, fais pas le⋅a malin⋅e.**

Dans ce cas, deux options : 

* l'option bourrin : sauvegarder ses fichiers dans un autre dossier, supprimer le répo local, re-cloner le répo distant (qui est donc maintenant dans sa dernière version), ré-appliquer ses modification, et faire un `git push`. Sale, mais efficace. 
* l'option propre, qu'on va décrire ci-dessous. 

Plusieurs étapes : 

1. si ce n'est pas déjà fait, faire un `git pull origin master`. Un message devrait t'avertir qu'il y a un conflit de version dans ton code.

2. avec `git status`, repérer quels fichiers causent problème (une mention « modifié des deux côtés » devrait s'afficher pour ces fichiers).

3. aller sur chacun des fichiers concernés, et repérer où on trouve les des caractères bizarres qui ressemblent à ça : 

   ```
   >>>>>>>>>>>>>>>>>>>>>> HEAD
   ... un morceau de code ici 
   <<<<<<<<<<<<<<<<<<<<<<
   ... un autre ici
   >>>>>>>>>>>>>>>>>>>>>>
   ```

   La structure est assez simple : entre la première et la deuxième ligne de « >>>> », il y a la dernière version du code telle qu'elle est renseignée sur le répo distant (la forge git). Entre la deuxième et la troisière ligne, il y a ta version locale du code. 

   Pour régler le problème de conflit, il suffit alors de supprimer la version qu'on ne souhaite pas garder, et de supprimer les lignes de « >>>>>> » pour ne garder que le code. 

4. Une fois toutes les modifications faites et les fichiers sauvegardés, faire un `git add .` Un petit coup de `git status` permet de s'assurer qu'on n'en a pas oublié. 

5. **Pousser les modification** comme indiqué sur le point 4. Mettre en message quelque chose comme « réglé les conflits de version et ajouté les points suivants  : ... », et pourquoi pas un petit mot d'insulte pour la personne qui ne t'as pas averti de ses modifications ? 



### 7. Aller plus loin avec git  

On a simplement effleuré toutes les possibilités de git. Il est possible d'avoir plusieurs versions du code alternatives en parallèle, de régler automatiquement les conflits de version, de nettoyer l'historique de version pour ne garder que des messages de commit clairs, de modifier un message de commit précédent, de rajouter un fichier qu'on avait oublié d'ajouter sur le commit précédent... et tellement plus encore. 

Mais pour ça, il y a un milliard de tutos sur le net mondial ! 



[^1]: il faut aussi noter que le HTML est un sous-ensemble du Markdown, c'est à dire que si vous insérez du code HTML directement dans le Markdown, ce sera accepté par les éditeurs. C'est parfois nécessaire, par exemple lorsque l'on veut mettre en page un texte d'une autre manière que celle qui est automatiquement générée par le générateur.  